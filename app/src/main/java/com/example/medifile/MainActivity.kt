package com.example.medifile

import android.app.DownloadManager
import android.content.Context
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.view.View
import android.widget.MediaController
import androidx.appcompat.app.AppCompatActivity
import com.example.medifile.databinding.ActivityMainBinding
import java.io.File

class MainActivity : AppCompatActivity() {

    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding
    private var handler: Handler = Handler()
    private var downloadmanager: DownloadManager? = null
    private val videoTitle = "plat145_1097.mp4"
    private val videoUrl =
        "http://ds.retail-soft.pro:51085/partner/4/api/1.0/player/421/file/plat145_1097"
    val image =
        "http://ds.retail-soft.pro:51085/partner/4/api/1.0/player/421/file/plat145_1094"
    private val html = "https://www.x5.ru/ru/"
    private val storageFileName = "plat145_1094.png"
    private val storageFileVideo = "plat145_1097.mp4"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        val storageImg = getFilePathByPartialName(this, storageFileName)
        val storageV = getFilePathByPartialName(this, storageFileVideo)

        if (intent.action == "DOWNLOAD_COMPLETE") {
            val filePath = intent.getStringExtra("FILE_PATH")
            if (filePath != null) {
                if (isImageFile(filePath)) {
                    val url = Uri.parse(filePath)
                    binding?.image?.visibility = View.VISIBLE
                    binding?.image?.setImageURI(url)
                } else {
                    binding?.textLoading?.visibility = View.GONE
                    binding?.image?.visibility = View.GONE
                    binding?.progress?.visibility = View.GONE
                    binding?.videoView?.visibility = View.VISIBLE
                    play(filePath)
                }
            }
        }
        downloadmanager = getSystemService(DOWNLOAD_SERVICE) as DownloadManager?
        showMedia(storageImg, storageV)
    }

    private fun showMedia(storageImg: String?, storageV: String?) {
        if (storageImg != null && storageV != null) {
            val urls = Uri.parse(storageImg)
            binding?.image?.visibility = View.VISIBLE
            binding?.image?.setImageURI(urls)
            Handler().postDelayed({
                playVideo(storageV)
            }, 2000)
        } else {
            downLoad(image, storageFileName)
        }
    }

    private fun registerDownloadReceiver(context: Context, downloadId: Long) {
        val filter = IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        context.registerReceiver(DownloadsBroadcastReceiver(), filter)
    }

    private fun play(videoUrl: String) {

        val mediaController = MediaController(this)
        val uri: Uri = Uri.parse(videoUrl)
        binding?.videoView?.setVideoURI(uri)
        mediaController.setAnchorView(binding?.videoView)
        mediaController.setMediaPlayer(binding?.videoView)
        binding?.videoView?.setMediaController(mediaController)

        binding?.videoView?.setOnCompletionListener {
            binding!!.videoView.visibility = View.GONE
            binding!!.web.visibility = View.VISIBLE
            showContent(videoUrl)
        }
        binding?.videoView?.start()
    }

    private fun downLoad(url: String, path: String) {

        val sharedPref =
            applicationContext.getSharedPreferences("DownloadStatus", Context.MODE_PRIVATE)
        val filePath = sharedPref.getString("filePath", null)

        if (filePath.isNullOrEmpty()) {

            val uri: Uri = Uri.parse(url)
            val request = DownloadManager.Request(uri)
            request.setTitle(path)
            request.setDescription("Downloading")
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            request.setVisibleInDownloadsUi(true)
                .setDestinationInExternalFilesDir(
                    this,
                    Environment.DIRECTORY_DOWNLOADS,
                    path
                )
            request.setAllowedOverMetered(true)
            val downLoad = downloadmanager!!.enqueue(request)
            registerDownloadReceiver(this, downLoad)

        } else {

            if (isImageFile(filePath)) {
                val editor = sharedPref.edit()
                val uri = Uri.parse(filePath)
                binding?.image?.visibility = View.VISIBLE
                binding?.image?.setImageURI(uri)
                editor.clear()
                editor.apply()
                downLoad(videoUrl, videoTitle)

            } else {

                binding?.image?.visibility = View.GONE
                binding?.textLoading?.visibility = View.GONE
                binding?.progress?.visibility = View.GONE
                binding?.videoView?.visibility = View.VISIBLE
                play(filePath)
            }


        }


    }

    private fun playVideo(storageV: String?) {

        binding?.textLoading?.visibility = View.GONE
        binding?.image?.visibility = View.GONE
        binding?.progress?.visibility = View.GONE
        binding?.videoView?.visibility = View.VISIBLE

        if (storageV != null) {
            play(storageV)
        }
    }

    private fun getFilePathByPartialName(context: Context, partialName: String): String? {
        val filePath =
            "///storage/emulated/0/Android/data/com.example.medifile/files/Download/$partialName"
        val file = File(filePath)
        var path: String? = null
        if (file.exists()) {
            path = filePath
        }
        return path
    }

    private fun isImageFile(filePath: String): Boolean {
        val imageExtensions = arrayOf("jpg", "jpeg", "png", "gif", "bmp")
        val fileExtension = getFileExtension(filePath)
        return imageExtensions.contains(fileExtension)
    }

    private fun getFileExtension(filePath: String): String {
        val lastDotIndex = filePath.lastIndexOf(".")
        return if (lastDotIndex != -1) {
            filePath.substring(lastDotIndex + 1)
        } else {
            ""
        }
    }

    private fun showContent(videoUrl: String) {

        binding?.image?.visibility = View.GONE
        binding?.videoView?.visibility = View.GONE
        binding?.web?.visibility = View.VISIBLE
        binding!!.web.loadUrl(html)

        handler.postDelayed({
            binding?.image?.visibility = View.VISIBLE
            binding?.web?.visibility = View.GONE
            handler.postDelayed({
                binding?.image?.visibility = View.GONE
                binding?.videoView?.visibility = View.VISIBLE
                binding?.videoView?.setVideoURI(Uri.parse(videoUrl))
                binding?.videoView?.setOnCompletionListener {
                    binding?.videoView?.visibility = View.GONE
                    showContent(videoUrl)
                }
                binding?.videoView?.start()
            }, 5000)
        }, 5000)
    }
}