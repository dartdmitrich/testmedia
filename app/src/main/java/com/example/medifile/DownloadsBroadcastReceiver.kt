package com.example.medifile

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class DownloadsBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {

        val downloadId = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
        if (downloadId != -1L) {

            val query = DownloadManager.Query()
            if (downloadId != null) {
                query.setFilterById(downloadId)
            }
            val downloadManager = context?.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            val cursor = downloadManager.query(query)
            if (cursor.moveToFirst()) {
                val columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)
                val filePath = cursor.getString(columnIndex)
                cursor.close()

                val broadcastIntent = Intent(context, MainActivity::class.java)
                broadcastIntent.action = "DOWNLOAD_COMPLETE"
                broadcastIntent.putExtra("FILE_PATH", filePath)

                context.startActivity(broadcastIntent)

                val sharedPref = context.getSharedPreferences("DownloadStatus", Context.MODE_PRIVATE)
                val editor = sharedPref.edit()
                editor.putString("filePath", filePath)
                editor.apply()
            }
        }
    }
}

